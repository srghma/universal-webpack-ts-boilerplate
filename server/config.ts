export const ServerConfig = {
  devServer: {
    host: 'localhost',
    port: 3000,
  },
  devClient: {
    host: 'localhost',
    port: 3001,
  },
  prod: {
    host: 'localhost',
    port: 8080,
  },
}
