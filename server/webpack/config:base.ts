import * as CircularDependencyPlugin from 'circular-dependency-plugin'
import * as webpack from 'webpack'
import getAbsolutePath from '../../utils/getAbsolutePath'

const baseConfig: webpack.Configuration = {
  context: getAbsolutePath(),

  module: {
    rules: [
      {
        exclude: /node_modules/,
        test: /\.tsx?$/,
        loaders: [
          'awesome-typescript-loader?configFileName=tsconfig.json',
        ],
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: './fonts/[hash].[ext]',
        },
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              gifsicle: {
                interlaced: false,
              },
              optipng: {
                optimizationLevel: 7,
              },
              pngquant: {
                quality: '65-90',
                speed: 4,
              },
              mozjpeg: {
                progressive: true,
                quality: 65,
              },
            },
          },
        ],
      },
    ],
  },

  /**
   * Output of bundles and assets
   */
  output: {
    path: getAbsolutePath('build/static'),
  },

  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      filename: '[name].js',
      minChunks: 3,
      name: 'common',
    }),
    new webpack.NamedModulesPlugin(),

    new CircularDependencyPlugin({
      exclude: /a\.js|node_modules/,
      failOnError: false,
    }),
  ],

  resolve: {
    alias: {
      server: getAbsolutePath('server'),
    },
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.css'],
  },
}

export default baseConfig
