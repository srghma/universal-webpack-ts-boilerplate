import { clientConfiguration, serverConfiguration } from 'universal-webpack'
import * as webpack from 'webpack'
import * as webpackMerge from 'webpack-merge'

import baseConfig from './config:base'
import devConfig from './config:dev'
import prodConfig from './config:prod'
import getAbsolutePath from '../../utils/getAbsolutePath'

export const universalWebpack = {
  server: {
    input:  getAbsolutePath('server/core/index.ts'),
    output: getAbsolutePath('build/server/index.js'),
  },
}

export default (config: string): webpack.Configuration => {
  const settings: string[] = config.split(':')
  const prod: boolean = settings[0] === 'prod'
  const env: string = settings[1]
  const isClient = env === 'client'

  const webpackConfig = webpackMerge(
    baseConfig,
    prod ?  prodConfig(isClient) : devConfig
  )

  /* ========================
      SET SERVER/CLIENT SIDE
     ======================== */
  switch (env) {
    case 'client': {
      return clientConfiguration(webpackConfig, universalWebpack)
    }
    case 'server': {
      return serverConfiguration(webpackConfig, universalWebpack)
    }
    default: {
      throw new Error(`wrong env ${env}`)
    }
  }
}
