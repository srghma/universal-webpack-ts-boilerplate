import * as webpack from 'webpack'
import { ServerConfig } from '../config'
import getAbsolutePath from '../../utils/getAbsolutePath'

const port = ServerConfig.devClient.port

const devConfig: webpack.Configuration = {
  devtool: 'inline-source-map',

  entry: {
    app: [
      getAbsolutePath('app'),
    ],
  },

  module: {
    rules: [
      {
        include: /node_modules/,
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            query: {
              sourceMap: true,
            },
          },
        ],
      }
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
      },
    }),
  ],

  /**
   * The output path of the bundle will be on the port of the dev server
   * The paths in the rendered HTML will point to the dev server.
   */
  output: {
    chunkFilename: '[name].[chunkhash].js',
    filename: '[name].bundle.js',
    publicPath: `http://localhost:${port}/static/`,
  },
}

export default devConfig
