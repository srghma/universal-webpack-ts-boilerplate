import { server } from 'universal-webpack'
import webpackConfig, { universalWebpack } from 'server/webpack/config'

// Start Server
server(webpackConfig('development'), universalWebpack)
