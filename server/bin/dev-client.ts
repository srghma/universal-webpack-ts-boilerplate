import * as express from 'express'
import * as webpack from 'webpack'
import * as webpackDevMiddleware from 'webpack-dev-middleware'
import * as webpackHotMiddleware from 'webpack-hot-middleware'

import { ServerConfig } from 'server/config'
import getWebpackConfig from 'server/webpack/config'
import invariant from 'utils/invariant'
import { get } from 'lodash-es'

const app = express()
const port = ServerConfig.devClient.port
const config = getWebpackConfig('development:client')
const compiler = webpack(config)

const maybePublicPath = get(config, ['output', 'publicPath'])
invariant(
  typeof maybePublicPath === 'string',
  'publicPath must be string',
  { maybePublicPath, config }
)
const publicPath = maybePublicPath as string // validation was successful

const options = {
  quiet: true,
  noInfo: true,
  hot: true,
  publicPath,
  headers: { 'Access-Control-Allow-Origin': '*' },
  stats: { colors: true },
}

app.use(webpackDevMiddleware(compiler, options))
app.use(webpackHotMiddleware(compiler))

app.listen(port, (error: Error) => {
  if (error) {
    console.error(error.stack || error)
    throw error
  }

  console.info('Webpack client bundle server listening on port %s', port)
})
