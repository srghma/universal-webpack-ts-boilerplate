import { NextFunction, Request, Response } from 'express'
import * as React from 'react'
import { renderToStaticMarkup } from 'react-dom/server'
import { Provider } from 'react-redux'
import { StaticRouter } from 'react-router-dom'
import { Chunks } from 'universal-webpack'

import Index from '~/pages'
import Html from './Html'
// import { configureStore } from '~/redux/create'
// import Routes from '~/routes'

export default function(chunks: Chunks) {
  return (req: Request, res: Response, next: NextFunction) => {
    const context: { url?: string } = {}
    // const store = configureStore()
    const store = {}

    const component = (
      <StaticRouter location={req.url} context={context}>
        <Index />
      </StaticRouter>
    )

    const state = {}

    const html = (
      <Html chunks={chunks} store={store} rootComponent={component} />
    )

    const result = renderToStaticMarkup(html)

    if (context.url) {
      // <Redirect> was used
      // TODO use RedirectWithStatus https://reacttraining.com/react-router/web/guides/server-rendering
      res.writeHead(301, {
        Location: context.url,
      })
      res.end()
    } else {
      res.write(`<!doctype html>\n${renderToStaticMarkup(html)}`)
      res.end()
    }
  }
}
