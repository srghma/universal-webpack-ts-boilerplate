import * as compression from 'compression'
import * as express from 'express'
import { Server as HttpServer } from 'http'
import { Parameters } from 'universal-webpack'

import { ServerConfig } from 'server/config'
import router from 'server/core/router'

export default function(params: Parameters): HttpServer {
  /**
   * Get ENV settings
   */
  const isProd = process.env.NODE_ENV === 'production'
  const port = isProd ? ServerConfig.prod.port : ServerConfig.devServer.port

  /**
   * Set a global var to allow us to check if we're server / client side.
   */
  if (global) {
    ;(global as any).__SERVER__ = true
  }

  /**
   * Set the app up
   */
  const app = express()

  app.use(compression())
  app.use(router(params.chunks()))

  return app.listen(port, () => {
    console.info('Server started on', port)
  })
}
