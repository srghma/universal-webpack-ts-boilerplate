import * as express from 'express'
import * as path from 'path'
import renderApp from './render-app'
import { Chunks } from 'universal-webpack'

export default (chunks: Chunks): express.Router => {
  const router = express.Router()

  router.use('/static', express.static(path.join(__dirname, '../static')))

  router.use('/', renderApp(chunks))

  return router
}
