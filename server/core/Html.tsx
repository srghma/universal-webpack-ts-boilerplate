import * as React from 'react'
import { renderToString } from 'react-dom/server'
import * as serialize from 'serialize-javascript'
import { Chunks } from 'universal-webpack'
import { Helmet } from 'react-helmet'

export interface HtmlProps {
  rootComponent: React.ReactElement<any>
  chunks: Chunks
  // store: Store<State.RootStateRecord>
  store: Object
}

const Html: React.SFC<HtmlProps> = ({ chunks, store, rootComponent }) => {
  const appString = renderToString(rootComponent)
  const stateString = `window.__INITIAL_STATE__=${serialize(store, {
    isJSON: true,
  })}`

  const helmet = Helmet.renderStatic() // NOTE: must go after renderToString
  const htmlAttrs = helmet.htmlAttributes.toComponent()
  const bodyAttrs = helmet.bodyAttributes.toComponent()

  return (
    <html {...htmlAttrs}>
      <head>
        {helmet.title.toComponent()}
        {helmet.meta.toComponent()}
        {helmet.link.toComponent()}
        <link rel="stylesheet" href={chunks.styles.app} />
      </head>
      <body {...bodyAttrs}>
        <div id="app" dangerouslySetInnerHTML={{ __html: appString }} />
        <script
          type="text/javascript"
          charSet="UTF-8"
          dangerouslySetInnerHTML={{ __html: stateString }}
        />
        <script
          type="text/javascript"
          charSet="UTF-8"
          src={chunks.javascript.app}
        />
      </body>
    </html>
  )
}

export default Html
