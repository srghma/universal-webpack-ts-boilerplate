import * as React from 'react'
import Logo from '~/components/Logo'

const style = `
  img {
    width: 300px;
    height: 300px;
    }
  h1 {
    font-family: Arial;
  }
  .myDiv {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100vh;
  }
`

const title = 'asdf'

export default function Index(): JSX.Element {
  return (
    <div>
      <div className="myDiv">
        <Logo />
        <img src="/static/pueue.png" />
        <h1>Welcome to Next.js + Typescript Boilerplate!!!</h1>
      </div>
      <style>
        {style}
      </style>
    </div>
  )
}
