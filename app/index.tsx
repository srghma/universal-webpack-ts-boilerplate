import 'babel-polyfill'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
// import { AppContainer } from 'react-hot-loader'
// import { Provider } from 'react-redux'
// import { ConnectedRouter } from 'react-router-redux'
// import createHistory from 'history/createBrowserHistory'

// import { State } from '../types'
// import { configureStore } from './store'
// import Routes from './routes'

// const initialState = {}
// const initialState: State.RootStateRecord = Transit.fromJSON<State.RootStateRecord>((window as any).__INITIAL_STATE__)
// const store = configureStore(initialState, history)

import Index from '~/pages'

function render(
  Component: React.ComponentClass<any> | React.StatelessComponent<any>
) {
  ReactDOM.render(<Component />, document.getElementById('app'))
}

// function render (
//   Component: React.ComponentClass<any> | React.StatelessComponent<any>
// ) {
//   ReactDOM.render(
//     <AppContainer>
//       <Provider store={store}>
//         <ConnectedRouter history={history}>
//           {Component}
//         </ConnectedRouter>
//       </Provider>
//     </AppContainer>,
//     document.getElementById('app'),
//   )
// }

render(Index)

// if (module.hot) {
//   module.hot.accept('./routes', () => {
//     const routes = require('./routes').default
//     render(routes)
//   })
// }
