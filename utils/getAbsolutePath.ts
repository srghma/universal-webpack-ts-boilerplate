import * as path from 'path'

const rootPath = path.resolve(__dirname, '..')

// get absolute path from path relative to root
export default function rootRelativePath(relativeToRoot?: string) {
  const absolute = path.resolve(rootPath, relativeToRoot || '')
  console.log(absolute)
  return absolute
}
